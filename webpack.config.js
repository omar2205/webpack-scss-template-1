const path = require('path');
const glob = require('glob');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	mode: 'production',
  entry: './src/index.js',
  output: {
    filename: 'bundle.[hash:6].js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
      rules: [{
          test: /\.scss$/,
          use: [
              {
                loader: MiniCssExtractPlugin.loader
              },
              {
                loader: "css-loader"
              },
              {
                loader: "sass-loader",
                options: {
                  includePaths: ['node_modules']
                    .map((d) => path.join(__dirname, d))
                    .map((g) => glob.sync(g))
                    .reduce((a, c) => a.concat(c), [])
                }
              }
          ],
      }]
  },
  plugins: [
    new MiniCssExtractPlugin({
	    // Options similar to the same options in webpackOptions.output
	    // both options are optional
	    filename: "[name].[hash:6].css",
	    chunkFilename: "[id].css"
    }),
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ]
};